#include "graph_list.h"

int main(){
  EdgeList*this;
  EDGE_SET_UINT32 ros=0,cols=0,ro_eg;
  char description[EDGE_SET_ITEM_LEN];
  EDGE_SET_UINT32 pairs;
  EDGE_SET_UINT32 *ro,*col;
  EDGE_SET_UINT32 i;
  fscanf(stdin,"%u",&ros);
  fscanf(stdin,"%u",&cols);
  fscanf(stdin,"%s",description);
  fscanf(stdin,"%u",&pairs);
  ro=(EDGE_SET_UINT32*)calloc(pairs,sizeof(EDGE_SET_UINT32));
  col=(EDGE_SET_UINT32*)calloc(pairs,sizeof(EDGE_SET_UINT32));
  this=edge_list_make(ros,cols,description);
  for(i=0;i<pairs;i++){
    fscanf(stdin,"%u",&ro[i]);
    fscanf(stdin,"%u",&col[i]);
    edge_list_on(this,ro[i],col[i]);
    assert(edge_list_is_on(this,ro[i],col[i])==EDGE_SET_GAIN);
  }
  ro_eg=ro[0];
  list_print(edge_list_ro(this,ro_eg));
  edge_list_off(this,ro[0],col[0]);
  assert(edge_list_is_on(this,ro[0],col[0])==EDGE_SET_FAIL);
  assert(edge_list_is_on(this,ro[1],col[1])==EDGE_SET_GAIN);
  list_print(edge_list_ro(this,ro_eg));
  edge_list_destroy(&this);
  free(ro);
  free(col);
  return 0;
}
