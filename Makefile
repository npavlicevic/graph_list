#
# Makefile 
# graph list
#

CC=gcc
AR=ar
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic -DMORE
OBJ_FLAGS=-c -static -g
AR_FLAGS=rcs
LIBS=-ltable -lm -lgraphsetq -lgraphset

FILES=graph_list.h graph_list.c 
FILES_ON_TST=${FILES} graph_list_elist_on_test.c 
FILES_BELLMAN_TST=${FILES} graph_list_elist_bellman_test.c 
FILES_BFS_TST=${FILES} graph_list_elist_bfs_test.c 
FILES_PATH_TST=${FILES} graph_list_elist_path_test.c 
FILES_RANDOM_TST=${FILES} graph_list_elist_random_test.c 
FILES_UNION_TST=${FILES} graph_list_elist_union_test.c 
FILES_INTERSECT_TST=${FILES} graph_list_elist_intersect_test.c 
FILES_INTERSECT_TST_TOO=${FILES} graph_list_elist_intersect_test_too.c 
FILES_COMPLEMENT_TST=${FILES} graph_list_elist_complement_test.c 
FILES_EIGHTK=${FILES} graph_list_elist_eightk_test.c
FILES_EIGHTKSPARSE=${FILES} graph_list_elist_eightksparse_test.c

all: main main_tst main_bellman main_bellman_tst main_bfs main_bfs_tst main_path main_path_tst main_random main_random_tst main_union main_union_tst main_intersect main_intersect_tst main_intersect_too main_intersect_tst_too main_complement main_complement_tst main_eightk tst_eightk main_eightksparse tst_eightksparse obj lib

main: ${FILES_ON_TST}
	${CC} ${CFLAGS} graph_list.c graph_list_elist_on_test.c ${LIBS} -o graph_list_elist_on_test

main_tst: ${FILES_ON_TST}
	./graph_list_elist_on_test < graph_list_elist_on_test.txt

main_bellman: ${FILES_BELLMAN_TST}
	${CC} ${CFLAGS} graph_list.c graph_list_elist_bellman_test.c ${LIBS} -o graph_list_elist_bellman_test

main_bellman_tst: ${FILES_BELLMAN_TST}
	./graph_list_elist_bellman_test < graph_list_elist_bellman_test.txt

main_bfs: ${FILES_BFS_TST}
	${CC} ${CFLAGS} graph_list.c graph_list_elist_bfs_test.c ${LIBS} -o graph_list_elist_bfs_test

main_bfs_tst: ${FILES_BFS_TST}
	./graph_list_elist_bfs_test < graph_list_elist_bfs_test.txt

main_path: ${FILES_PATH_TST}
	${CC} ${CFLAGS} graph_list.c graph_list_elist_path_test.c ${LIBS} -o graph_list_elist_path_test

main_path_tst: ${FILES_PATH_TST}
	./graph_list_elist_path_test < graph_list_elist_path_test.txt

main_random: ${FILES_RANDOM_TST}
	${CC} ${CFLAGS} graph_list.c graph_list_elist_random_test.c ${LIBS} -o graph_list_elist_random_test

main_random_tst: ${FILES_RANDOM_TST}
	./graph_list_elist_random_test < graph_list_elist_random_test.txt

main_union: ${FILES_UNION_TST}
	${CC} ${CFLAGS} graph_list.c graph_list_elist_union_test.c ${LIBS} -o graph_list_elist_union_test

main_union_tst: ${FILES_UNION_TST}
	./graph_list_elist_union_test < graph_list_elist_union_test.txt

main_intersect: ${FILES_INTERSECT_TST}
	${CC} ${CFLAGS} graph_list.c graph_list_elist_intersect_test.c ${LIBS} -o graph_list_elist_intersect_test

main_intersect_tst: ${FILES_INTERSECT_TST}
	./graph_list_elist_intersect_test < graph_list_elist_intersect_test.txt

main_intersect_too: ${FILES_INTERSECT_TST_TOO}
	${CC} ${CFLAGS} graph_list.c graph_list_elist_intersect_test_too.c ${LIBS} -o graph_list_elist_intersect_test_too

main_intersect_tst_too: ${FILES_INTERSECT_TST_TOO}
	./graph_list_elist_intersect_test_too < graph_list_elist_intersect_test_too.txt

main_complement: ${FILES_COMPLEMENT_TST}
	${CC} ${CFLAGS} graph_list.c graph_list_elist_complement_test.c ${LIBS} -o graph_list_elist_complement_test

main_complement_tst: ${FILES_COMPLEMENT_TST}
	./graph_list_elist_complement_test < graph_list_elist_complement_test.txt

main_eightk: ${FILES_EIGHTK}
	${CC} ${CFLAGS} graph_list.c graph_list_elist_eightk_test.c ${LIBS} -o graph_list_elist_eightk_test

tst_eightk: ${FILES_EIGHTK}
	./graph_list_elist_eightk_test < graph_list_elist_eightk_test.txt

main_eightksparse: ${FILES_EIGHTKSPARSE}
	${CC} ${CFLAGS} graph_list.c graph_list_elist_eightksparse_test.c ${LIBS} -o graph_list_elist_eightksparse_test

tst_eightksparse: ${FILES_EIGHTKSPARSE}
	./graph_list_elist_eightksparse_test < graph_list_elist_eightksparse_test.txt

obj: ${FILES}
	${CC} ${OBJ_FLAGS} graph_list.c -o graph_list.o

lib: ${FILES}
	${AR} ${AR_FLAGS} libgraphlist.a graph_list.o
